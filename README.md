# README #

### TODO ###

* Convert the Projection Matrix and Eye To Head matrix in the Ogre Matrix format.
* Test if I need the Eye To Head matrix and the splitting of the screen ( I'm not sure if I have done this right so far but this is best tested when the demo is running so I will test this later).
* Compositor on this part I'm a bit clueless. The Openvr documentation says me I should have a loop that does the following things
>while Running:
>    WaitGetPoses
>
>    Render Left and Right cameras
>
>    Submit Left and Right render targets
>
>    Update game logic

think I have to do this whit the Compositor but I am neither sure of this nor do I have a clue how to do this. 
Read more about that at: https://github.com/ValveSoftware/openvr/wiki/IVRCompositor_Overview

* write the actual demo (should be fairly easy when the stuff whit Openvr is done)
* add vive/Openvr controller support ( including ways to change there appearance in the game ) In my understanding Openvr renders them by itself and I have to give it the model.
* Make everything work on Windows it seams like Openvr is currently not working on linux ( this should change soon though ). My first attempt was on Windows but so far I couldn’t make it work.



### Contribution ###

Help is very welcome 

* write me a message
* make a pull request
* make a issue
* or write me in the ogre forum

I will try to respond as fast as I can



### What is this repository for? ###

* Demo for Ogre working with OpenVR
* Version 0.1



### How do I get set up? ###

* `git clone https://bitbucket.org/GamligerBrokkoli/ogre_openvr`
* set two variables `OGRE_HOME` and `OPENVR_HOME` than export them

	OGRE_HOME pointing to Ogre with the build directory inside

	OPENVR_HOME to the OpenVR sdk directory you can get it here https://github.com/ValveSoftware/openvr 

* `cd ogre_openvr/Ogre_OpenVR/`
* `mkdir build`
* `cd build`
* `cmake ..` or `cmake -G "your favorit IDE" ..` 

	if you want to use something else than make  
	( look with `cmake --help` if a generator for your IDE exists ) 
	example: `cmake -G "CodeBlocks - Unix Makefiles" ..` 

* `make all`
* or use your IDE if you generated a other project file with cmake



### Who do I talk to? ###

* me (GamligerBrokkoli) 
* AltesToast in Ogre forum