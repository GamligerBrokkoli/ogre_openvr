/// Copyright (C) 2016 Jonas R�ther
///
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
/// to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
/// and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
///
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
///
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
/// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
/// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
/// DEALINGS IN THE SOFTWARE.


#pragma once


#include <openvr.h>

#include <string>
#include <OgreSceneManager.h>
#include <OgreRenderWindow.h>

// define for Debuging
#define DB


namespace Ogre
{
	class SceneManager;
	class RenderWindow;
	class Camera;
	class SceneNode;
	class Viewport;
	class CompositorInstance;
}

class Ogre_OpenVR
{
public:

    enum EYE_SPACE
    {
        LEFT = 0,
        RIGTH = 1
    };

	Ogre_OpenVR(void);
	~Ogre_OpenVR(void);
	bool setupOpenVR();
	bool setupOgre(Ogre::SceneManager *sm, Ogre::RenderWindow *win, Ogre::SceneNode *parent = 0);
	void shutDownOpenVR();
	void shutDownOgre();
	bool isOgreReady() const;
	bool isOpenVRReady() const;

	/// Update camera node using current OpenVR orientation.
	void update();

	/// Reset orientation of the sensor.
	void resetOrientation();

	/// Retrieve the SceneNode that contains the two cameras used for stereo rendering.
	Ogre::SceneNode *getCameraNode(EYE_SPACE i);

	/// Retrieve the current orientation of the OpenVR HMD.
//	Ogre::Quaternion getOrientation() const;

	/// Retrieve either of the two distortion compositors.
	Ogre::CompositorInstance *getCompositor(EYE_SPACE i);

	/// Retrieve either of the two cameras.
	Ogre::Camera *getCamera(EYE_SPACE i);

	/// Retrieve either of the two viewports.
	Ogre::Viewport *getViewport(EYE_SPACE i);


protected:

    //OpenVR paramethers
	vr::IVRSystem* m_hmd = nullptr;
	uint32_t m_hmdWidth;
	uint32_t m_hmdHeight;
	bool m_OpenVRReady;		/// Has the OpenVR rift been fully initialised?

    //Ogre paramethers
	bool m_ogreReady;		/// Has ogre been fully initialised?
	Ogre::SceneManager *m_sceneManager;
	Ogre::RenderWindow *m_window;
	Ogre::SceneNode *m_cameraNode;

    Ogre::Camera *m_cameras[2];
	Ogre::Viewport *m_viewports[2];
	Ogre::CompositorInstance *m_compositors[2];

private:

    #ifdef DB
        std::string getHMDString(vr::IVRSystem* pHmd, vr::TrackedDeviceIndex_t unDevice, vr::TrackedDeviceProperty prop, vr::TrackedPropertyError* peError = nullptr);
    #endif // DB

};


