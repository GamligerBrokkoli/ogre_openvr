/// Copyright (C) 2016 Jonas R�ther
///
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
/// to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
/// and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
///
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
///
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
/// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
/// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
/// DEALINGS IN THE SOFTWARE.

#include "Ogre_OpenVR.h"
#include <OgreRenderWindow.h>
#include <OgreCompositorManager.h>
#include <OgreCompositorInstance.h>
#include <OgreCompositionTargetPass.h>
#include <OgreCompositionPass.h>
#include <OgreLogManager.h>
#include <OgreManualObject.h>
#include <OgreCamera.h>
#include <OgreSceneNode.h>


using namespace std;


namespace
{
	const float g_defaultNearClip = 0.01f;
	const float g_defaultFarClip = 10000.0f;
}


Ogre_OpenVR::Ogre_OpenVR(void):
    m_hmd(nullptr),
    m_OpenVRReady(false),
    m_ogreReady(false),
    m_window(nullptr),
    m_sceneManager(nullptr),
    m_cameraNode(nullptr),
    m_cameras{nullptr, nullptr},
    m_viewports{nullptr, nullptr},
    m_compositors{nullptr, nullptr} {}


bool Ogre_OpenVR::setupOpenVR()
{
	if(m_OpenVRReady)
	{
	    #ifdef DB
		Ogre::LogManager::getSingleton().logMessage("OpenVR: Already Initialised");
		#endif // DB
		return true;
	}

	#ifdef DB
	Ogre::LogManager::getSingleton().logMessage("OpenVR: Initialising system");
	#endif // DB

	vr::EVRInitError eError = vr::VRInitError_None;
	vr::IVRSystem* m_hmd = vr::VR_Init(&eError, vr::VRApplication_Scene);

	if (eError != vr::VRInitError_None) {
        #ifdef DB
        Ogre::LogManager::getSingleton().logMessage( string("OpenVR Initialization Error: ") + vr::VR_GetVRInitErrorAsEnglishDescription(eError));
        #endif // DB
        return false;
	}

	// get resolution of hmd
	m_hmd->GetRecommendedRenderTargetSize(&m_hmdWidth, &m_hmdHeight);

	#ifdef DB
	const std::string& driver = getHMDString(m_hmd, vr::k_unTrackedDeviceIndex_Hmd, vr::Prop_TrackingSystemName_String);
	const std::string& model  = getHMDString(m_hmd, vr::k_unTrackedDeviceIndex_Hmd, vr::Prop_ModelNumber_String);
	const std::string& serial = getHMDString(m_hmd, vr::k_unTrackedDeviceIndex_Hmd, vr::Prop_SerialNumber_String);
    const float freq = m_hmd->GetFloatTrackedDeviceProperty(vr::k_unTrackedDeviceIndex_Hmd, vr::Prop_DisplayFrequency_Float);

    Ogre::LogManager::getSingleton().logMessage( string("HMD: \n") +
                                                 "TrackingSystemName: " + driver + "\n" +
                                                 "ModelNumber: " + model + "\n"  +
                                                 "SerialNumber: " + serial + "\n" +
                                                 "DisplayFrequency: " + to_string( freq ) + "\n" +
                                                 "DisplayWidth: " + to_string( m_hmdWidth ) + "\n"  +
                                                 "DisplayHeight: " + to_string( m_hmdHeight ) + "\n\n" );
	#endif // DB

	m_OpenVRReady = true;

	#ifdef DB
    Ogre::LogManager::getSingleton().logMessage("OpenVR: OpenVR setup completed successfully");
	#endif // DB

	return true;
}

bool Ogre_OpenVR::setupOgre(Ogre::SceneManager *sceneManager, Ogre::RenderWindow *window, Ogre::SceneNode *parent)
{
	m_window = window;
	m_sceneManager = sceneManager;
	#ifdef DB
    Ogre::LogManager::getSingleton().logMessage("OpenVR: Setting up Ogre");
    #endif // DB

	if(parent)
		m_cameraNode = parent->createChildSceneNode("StereoCameraNode");
	else
		m_cameraNode = sceneManager->getRootSceneNode()->createChildSceneNode("StereoCameraNode");

	m_cameras[EYE_SPACE::LEFT] = sceneManager->createCamera("CameraLeft");
	m_cameras[EYE_SPACE::RIGTH] = sceneManager->createCamera("CameraRight");


    // Set Clip Distance
    m_cameras[EYE_SPACE::LEFT]->setNearClipDistance(g_defaultNearClip);
    m_cameras[EYE_SPACE::LEFT]->setFarClipDistance(g_defaultFarClip);
    m_cameras[EYE_SPACE::RIGTH]->setNearClipDistance(g_defaultNearClip);
    m_cameras[EYE_SPACE::RIGTH]->setFarClipDistance(g_defaultFarClip);


    //Projection Matrix I think the camera position is included here ( but I'm not sure )
    const vr::HmdMatrix44_t& ltProj = m_hmd->GetProjectionMatrix(vr::Eye_Left,  g_defaultNearClip, g_defaultFarClip, vr::API_OpenGL);
    const vr::HmdMatrix44_t& rtProj = m_hmd->GetProjectionMatrix(vr::Eye_Right, g_defaultNearClip, g_defaultFarClip, vr::API_OpenGL);
    // convert matrix here
    //m_cameras[EYE_SPACE::LEFT]->setCustomProjectionMatrix(ltProj);
    //m_cameras[EYE_SPACE::RIGTH]->setCustomProjectionMatrix(rtProj);


    //Eye To Head matrix (I think this makes the the cameras small so that 2 cameras fit on one viewport)
    const vr::HmdMatrix34_t& ltMatrix = m_hmd->GetEyeToHeadTransform(vr::Eye_Left);
    const vr::HmdMatrix34_t& rtMatrix = m_hmd->GetEyeToHeadTransform(vr::Eye_Right);
    // convert matrix here
    //m_cameras[EYE_SPACE::LEFT]->setCustomViewMatrix(ltMatrix);
    //m_cameras[EYE_SPACE::RIGTH]->setCustomViewMatrix(rtMatrix);


    // Add the cameras to a viewport (I'm not sure but I think I could also add both cameras to one viewport) will try to do it whit 2 for now
    m_viewports[EYE_SPACE::LEFT] = m_window->addViewport(m_cameras[EYE_SPACE::LEFT], 0, 0.0f, 0, 0.5f, 1.0f);
    m_viewports[EYE_SPACE::RIGTH] = m_window->addViewport(m_cameras[EYE_SPACE::RIGTH], 1, 0.5f, 0, 0.5f, 1.0f);



    //for this I think I need a Compositor ( I currently don't know how I should do this) maybe overwrite some function in a class that inherits from compositor.
    //vr::VRCompositor()->Submit(vr::Eye_Left, Texture);
    //vr::VRCompositor()->Submit(vr::Eye_Right, Texture);

	m_OpenVRReady = true;

	Ogre::LogManager::getSingleton().logMessage("OpenVR: OpenVR setup completed successfully");
	return true;
}

Ogre_OpenVR::~Ogre_OpenVR(void)
{
	shutDownOgre();
	shutDownOpenVR();
}

void Ogre_OpenVR::shutDownOpenVR()
{
	m_OpenVRReady = false;
    vr::VR_Shutdown();
    m_hmd = nullptr;
}

void Ogre_OpenVR::shutDownOgre()
{
	m_ogreReady = false;
	for(int i=0;i<2;++i)
	{
		if(m_compositors[i])
		{
			Ogre::CompositorManager::getSingleton().removeCompositor(m_viewports[i], "OpenVR");
			m_compositors[i] = 0;
		}
		if(m_viewports[i])
		{
			m_window->removeViewport(i);
			m_viewports[i] = 0;
		}
		if(m_cameras[i])
		{
			m_cameras[i]->getParentSceneNode()->detachObject(m_cameras[i]);
			m_sceneManager->destroyCamera(m_cameras[i]);
			m_cameras[i] = 0;
		}
	}
	if(m_cameraNode)
	{
		m_cameraNode->getParentSceneNode()->removeChild(m_cameraNode);
		m_sceneManager->destroySceneNode(m_cameraNode);
		m_cameraNode = 0;
	}
	m_window = 0;
	m_sceneManager = 0;
}

bool Ogre_OpenVR::isOpenVRReady() const
{
	return m_OpenVRReady;
}

bool Ogre_OpenVR::isOgreReady() const
{
	return m_ogreReady;
}


void Ogre_OpenVR::update()
{
}

Ogre::SceneNode *Ogre_OpenVR::getCameraNode(EYE_SPACE i)
{
	return m_cameraNode;
}

//Ogre::Quaternion Ogre_OpenVR::getOrientation() const
//{
//}

Ogre::CompositorInstance *Ogre_OpenVR::getCompositor(EYE_SPACE i)
{
	return m_compositors[i];
}

#ifdef DB
string Ogre_OpenVR::getHMDString(vr::IVRSystem* pHmd, vr::TrackedDeviceIndex_t unDevice, vr::TrackedDeviceProperty prop, vr::TrackedPropertyError* peError /*= nullptr*/ )
{
	uint32_t unRequiredBufferLen = pHmd->GetStringTrackedDeviceProperty(unDevice, prop, nullptr, 0, peError);
	if (unRequiredBufferLen == 0) {
	    return "";
    }

	char* pchBuffer = new char[unRequiredBufferLen];
	unRequiredBufferLen = pHmd->GetStringTrackedDeviceProperty(unDevice, prop, pchBuffer, unRequiredBufferLen, peError);
	std::string sResult = pchBuffer;
	delete[] pchBuffer;

	return sResult;
}
#endif // DB
